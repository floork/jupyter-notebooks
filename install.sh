#!/bin/bash

git clone https://github.com/floork/jupyter-notebooks/

wait 1

cd ./jupyter-notebooks

bash ./start.sh

echo "Do you want to remove the jupyter-notebooks folder? (y/n)"
read answer

if [ "$answer" == "y" ] || [ "$answer" == "Y" ]; then
    cd ..
    rm -rf ./jupyter-notebooks
fi
